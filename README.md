# Yew test

Yew is a rust library for making html webpages, like react or vue, 
but in rust and instead of generating javascript code, it gets compiled into Web Assembly.


## Build

`wasm-pack build --target web --out-name wasm --out-dir ./static`

## Serve
I used miniserve for serving.
`miniserve ./static --index index.html`

# Docs
Yew
https://yew.rs/docs/en/getting-started/build-a-sample-app/


CSS Framework
https://minstyle.io/docs/


