use yew::prelude::*;

pub struct BinaryConverter {
    link: ComponentLink<Self>,
    text: String,
    binary_form: String,
}

pub enum Msg {
    EnterValue(String),
    Convert,
}

impl Component for BinaryConverter {
    type Message = Msg;
    type Properties = ();

    fn create(props: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
            text: "".to_owned(),
            binary_form: "burg".to_owned(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::EnterValue(val) => {
                self.text = val
            }
            Msg::Convert => {
                let bytes = self.text.as_bytes();
                self.binary_form = bytes.into_iter().fold("".to_owned(),
                |acc, byte| format!("{}0{:b}", acc, byte)
                 );
            }
        }
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        true
    }

    fn view(&self) -> Html {
        html!{
            <div>
                <h3>{ "Convert text to binary" }</h3>
                <span>
                    <input type="text" oninput=self.link.callback(|e: InputData| Msg::EnterValue(e.value))/>
                </span>
                <span>
                    <p>{ &self.binary_form }</p>
                </span>
                <button class="ms-btn" onclick=self.link.callback(|_| Msg::Convert)>{ "Convert" }</button>
            </div>
        }
    }
}
