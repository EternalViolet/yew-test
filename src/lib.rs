mod components;

use wasm_bindgen::prelude::*;
use yew::prelude::*;
use components::binary_converter::BinaryConverter;

struct Model {
    link: ComponentLink<Self>,
}


impl Component for Model {
    type Message = ();
    type Properties = ();
    
    /// This is the first thing that gets called on component creation.
    fn create(_: Self::Properties, link: ComponentLink<Self>) -> Self {
        Self {
            link,
        }
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        true
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        // Should only return "true" if new properties are different to
        // previously received properties.
        // This component has no properties so we will always return "false".
        false
    }

    fn view(&self) -> Html {
        html! {
            <html>
                <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/minstyle.io@1.1.0/css/minstyle.io.css"/>
                <body>
                    <div>
                        <h1>{ "Nice helpers" }</h1>
                        <BinaryConverter />
                    </div>
                </body>
            </html>
        }
    }
}

#[wasm_bindgen(start)]
pub fn run_app() {
    App::<Model>::new().mount_to_body();
}
